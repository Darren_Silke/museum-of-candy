# Museum of Candy

Museum of Candy is a simple one-page responsive website for a fictitious candy museum. It was built using [Bootstrap](https://getbootstrap.com) in conjunction with HTML, CSS and JavaScript.

A live version of Museum of Candy can be viewed [here](https://thecandymuseum.netlify.app).

## Running Locally

Clone the repository and open the 'index.html' file in your Web browser.
